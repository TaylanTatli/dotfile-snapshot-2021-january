---
title: Finding a phone that sucksless
---

All of those crazy features on a platform that is under my control - free open-source software. I'll pay for it, monthly even to support the developers in order to keep such a device and software running into the future.


**Phones I've tried or spark an interest:**

- The Light Phone II
  - Does not yet feaature a Swedish or Scandinavian keyboard layout (as of June '20)
  - Closed source with an open SDK to come (as of June '20).
  - Some bug causing it to not work properly with Swedens larges mobile network provider.
- Punkt MP02
  - Amazingly sluggish OS (based on ASOP 8.1).
  - Closed source.
- WiPhone
  - Open-source-ish?
  - Extremely underwhelming hardware specs.
- Nokia 5310 (2020)
  - Closed source.
  - Non-KaiOS, which is a good thing.


Currently postmarketOS seem to be our only hope. Here's the most intriguing interface I've ever seen on a portable device: [```sxmo```](https://sr.ht/~mil/Sxmo/) - *"Simple X Mobile"*.

If a BlackBerry had a child with a low-power 400x640 resolution 2" display and a respectable camera and the interface was mainly a curses/dialog/whiptail menues - yum!

All I want is:

- **Threaded messages**
- **Calls**
- **Contacts**
- Calendar (schedule)
- Camera
- Music
- Podcasts
- Terminal emulator

----

## How's the qwerty phone market these days?

Wonder if there's a qwerty phone able to boot postmarketOS?




