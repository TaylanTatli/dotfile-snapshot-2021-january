micro-zigfmt
============

Plugin for the [micro editor](https://github.com/zyedidia/micro) that provides support for:

- `zig fmt` on save
- `zig fmt --check` integration with the official linter plugin

## Installation

Install by cloning this into your plug directory:

```
git clone https://github.com/squeek502/micro-zigfmt ~/.config/micro/plug/zigfmt
```

## Usage

See [the help file](help/zigfmt.md)

Or, after installation, from within micro:

```
> help zigfmt
```
